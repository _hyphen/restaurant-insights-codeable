CREATE TABLE "Client" (
  id SERIAL PRIMARY KEY,
  name VARCHAR(25) UNIQUE NOT NULL,
  age INTEGER  CHECK (age >= 18) NOT NULL, 
  gender CHAR(1) CHECK (gender IN ('m', 'f', 'M', 'F')) NOT NULL, 
  nationality VARCHAR(30) NOT NULL, 
  occupation VARCHAR(25) NOT null
);

CREATE TABLE "Restaurant" (
  id SERIAL UNIQUE,
  city VARCHAR(25) NOT NULL, 
  name VARCHAR(25) NOT NULL, 
  address VARCHAR(50),
  type VARCHAR(25) NOT NULL, 
  PRIMARY KEY (id, address)
);

CREATE TABLE "Dish"(
  id SERIAL UNIQUE, 
  rest_id INTEGER REFERENCES "Restaurant"(id) NOT NULL,
  name VARCHAR(50) NOT NULL,
  price INTEGER NOT NULL, 
  PRIMARY KEY (rest_id, name, price)
);

CREATE TABLE "Visit" (
  id SERIAL PRIMARY KEY, 
  client_id INTEGER  REFERENCES "Client" (id) NOT NULL,
  rest_id INTEGER  REFERENCES "Restaurant" (id) NOT NULL,
  dish_id INTEGER  REFERENCES "Dish" (id) NOT NULL, 
 	"date" DATE NOT NULL
);


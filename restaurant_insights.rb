# frozen_string_literal: true

require 'readline'
require 'cli'
require_relative 'queries'
require 'terminal-table'

WELCOME_MSG = <<~mark
  Welcome to the Restaurants Insights - 2020!
  Write 'menu' at any moment to print the menu again.
  ---
  1. List of restaurants included in the research filter by ['' | --type=string | --city=string]
  2. List of unique dishes included in the research
  3. Number and distribution (%) of users by [--group=[age | gender | occupation | nationality]]
  4. Top 10 restaurants by the number of visitors.
  5. Top 10 restaurants by the sum of sales.
  6. Top 10 restaurants by the average expense of their users.
  7. The average consumer expense group by [--group=[age | gender | occupation | nationality]]
  8. The total sales of all the restaurants group by month [--order=asc]]
  9. The list of dishes and the restaurant where you can find it at a lower price.
  10. The favorite dish for [--age=number | --gender=string | --occupation=string | --nationality=string]
  ---
  Pick a number from the list and an [option] if neccesary
mark

prompt = '$ '
PAT_GROUP = /(?<option>group)=(?<value>(?:age|gender|occupation|nationality))/.freeze
PAT_AGE = /(?<option>age)=(?<value>\d{2})/.freeze
PAT_GENDER = /(?<option>gender)=(?<value>(?:male|female|Male|Female|M|F))/.freeze
PAT_STRING = /(?<option>(?:occupation|nationality))=(?<value>[a-zA-Z ]+)/.freeze

OPT_1 = /(?<num>1)(?: --(?<option>(?:type|city))=(?<value>[a-zA-Z ]+))?/.freeze
NON_OPT = /(?<num>[24569])/.freeze
OPT_GROUP = /(?<num>[37]) --#{PAT_GROUP}/.freeze
OPT_8 = /(?<num>8)(?: --(?<option>order)=(?<value>(?:asc|desc)))?/.freeze
OPT_10 = /(?<num>10) --(?:#{PAT_AGE}|#{PAT_GENDER}|#{PAT_STRING})/.freeze
NAV_OPT = /(?<option>(?:menu|exit))/.freeze

OPTIONS = /(?:#{NAV_OPT}|#{OPT_1}|#{NON_OPT}|#{OPT_GROUP}|#{OPT_8}|#{OPT_10})/.freeze

db_name = File.open('db_name.sql') { |f| f.read}
database = RestaurantQueries.new(db_name)

puts WELCOME_MSG

while command_line = Readline.readline(prompt, true)

  captures = command_line.strip.match(/^#{OPTIONS}$/)
  if captures.nil?
    puts "Error: Wrong pattern! Try again.\nWrite 'menu' or 'exit'."
    next
  end

  option = captures['option']

  (puts WELCOME_MSG; next) if option == 'menu'
  exit(1) if option == 'exit'

  value = captures['value']

  case captures['num'].to_i
  when 1
    database.list_unq_restaurant(option: option, value: value)
  when 2
    database.list_unq_dish
  when 3
    database.list_client_group_by(value: value)
  when 4
    database.top10_by_visits
  when 5
    database.top10_by_total_sales
  when 6
    database.top10_by_avg_expenses
  when 7
    database.avg_expense_group_by(value: value)
  when 8 
    value = 'asc' if value.nil?
    database.total_sales_by_month(value: value)
  when 9
    database.list_cheapest_dishes
  when 10
    database.favorite_dish(option: option, value: value)
  end
end

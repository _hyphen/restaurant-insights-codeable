# Restaurants Insights

### Tasks

#### 1. Create a Entity Relationship Diagram (ERD)

At this point, it should be quite obvious that the current CSV file is not
normalized. It is just one single table with a lot of repeated data.

Take a moment with your team to analize and normalize this table and prepare an
ERD which shows `tables`, `columns`, `columns_types`, `constrains` and
`relationships`. You can use any software you want for this. Even pencil and
paper would be enough.

You should include an image of your ERD (png, jpg, pdf) on your solution.

#### 2. Create the database and tables

Using Data Definition Language (DDL) statements, create the database and tables
according to your Entity Relationship Diagram.

Put all your SQL code inside a file named `create.sql`. Check the orden of your
commands! For example you can't `REFERENCES "user"(id)` if the table "user"
doesn't exists yet.

If your `create.sql` file works well, you should be able to delete your entire
database and create it again doing something similar to:

```bash
$ dropdb my_db_name
$ createdb my_db_name
$ psql -d my_db_name < create.sql
BEGIN
CREATE TABLE
CREATE TABLE
....
COMMIT
$
```

Include the `create.sql` file on your solution.

#### 3. Populate your tables with the Insight-Restaurant-2019 data

The `data.csv` file has almost 88.5K registers. Insert that amount of data
manually is not an option. Create a ruby application to upload all the records
to your database. Remember that this time, we don't have one CSV file per table
so you will need to handle the current file to extract the relevant information
for each table and upload them.

The program should be called `insert_data.rb` and should take two arguments
`database_name` and `csv_file_path`.

The expected output should be similar to:

```bash
$ ruby insert_data.rb db_name /csv/path
Inserting data into [table_name]...
400 rows inserted correctly.
100 rows not inserted.
Inserting data into [another_table_name]...
xx rows inserted correctly.
xx rows not inserted.
...
$
```

Include the `insert_data.rb` file on your solution.

#### 4. CLI app to interact with Insight-Restaurant-2019

Now we have the database ready to start receiving queries. The research team has
required the development of a CLI to share some insights about the Restaurant
consumption data with our clients.

> All the example tables are using dummy data. Their numbers do not represent
> the expected result, just the expected format.

**Client can see a menu**

As a client, I want to run the application and see a menu with a list of options
so that I could choose the topic I most interested in.

- When I run the command `ruby restaurant-insights.rb` I see a welcome message
- And below the welcome message I see a list with 10 options
- And I'm prompt to choose on option with the message
  `Pick a number from the list and an [option] if neccesary`
- And if a write 'menu' the Welcome message and menu is printed again.

```bash
$ ruby restaurant-insights.rb
Welcome to the Restaurants Insights - 2019!
Write 'menu' at any moment to print the menu again.
---
1. List of restaurants included in the research filter by ['' | --type=string | --city=string]
2. List of unique dishes included in the research
3. Number and distribution (%) of users by [--group=[age | gender | occupation | nationality]]
4. Top 10 restaurants by the number of visitors.
5. Top 10 restaurants by the sum of sales.
6. Top 10 restaurants by the average expense of their users.
7. The average consumer expense group by [--group=[age | gender | occupation | nationality]]
8. The total sales of all the restaurants group by month [--order=asc]]
9. The list of dishes and the restaurant where you can find it at a lower price.
10. The favorite dish for [--age=number | --gender=string | --occupation=string | --nationality=string]
---
Pick a number from the list and an [option] if neccesary
>
```

**Client can see the list of restaurants**

As a client I want to use the option #1 of the menu so that I can see the list
of restaurants filter by type or city.

Usage:

```bash
Welcome to the Restaurants Insights - 2019!
Write 'menu' at any moment to print the menu again.
---
1. List of restaurants included in the research filter by ['' | --type=string | --city=string]
2. List of unique dishes included in the research
3. Number and distribution (%) of users by [--group=[age | gender | occupation | nationality]]
4. Top 10 restaurants by the number of visitors.
5. Top 10 restaurants by the sum of sales.
6. Top 10 restaurants by the average expense of their users.
7. The average consumer expense group by [--group=[age | gender | occupation | nationality]]
8. The total sales of all the restaurants group by month [--order=asc]]
9. The list of dishes and the restaurant where you can find it at a lower price.
10. The favorite dish for [--age=number | --gender=string | --occupation=string | --nationality=string]
---
Pick a number from the list and an [option] if neccesary
# Without filter option
>1
+-----------------------------------------------------+
|                 List of restaurants                 |
+----------------------+------------+-----------------+
| name                 | type       | city            |
+----------------------+------------+-----------------+
| Blue Plate Brasserie | Senegalese | Belvastad       |
+----------------------+------------+-----------------+
| Fast Curry           | Brazilian  | Rohanberg       |
+----------------------+------------+-----------------+
| Golden Bar & Grill   | Korean     | South Saranstad |
+----------------------+------------+-----------------+
| ...                  | ...        | ...             |
+----------------------+------------+-----------------+

# With filter type
>1 --type=Korean
+-----------------------------------------------+
|              List of restaurants              |
+--------------------+--------+-----------------+
| name               | type   | city            |
+--------------------+--------+-----------------+
| Golden Bar & Grill | Korean | South Saranstad |
+--------------------+--------+-----------------+
| Blue Plate Diner   | Korean | South Hilde     |
+--------------------+--------+-----------------+
| Salty Bar & Grill  | Korean | Rohanberg       |
+--------------------+--------+-----------------+
| ...                | ...    | ...             |
+--------------------+--------+-----------------+
# With filter city
>1 --city="South Hilde"
+------------------------------------------+
|            List of restaurants           |
+---------------+------------+-------------+
| name          | type       | city        |
+---------------+------------+-------------+
| Orange BBQ    | German     | South Hilde |
+---------------+------------+-------------+
| Golden Burger | Senegalese | South Hilde |
+---------------+------------+-------------+
| Big Eats      | Vegetarian | South Hilde |
+---------------+------------+-------------+
| ...           | ...        | ...         |
+---------------+------------+-------------+
```

**Client can see the list of unique dishes**

As a client I want to use the option #2 of the menu so that I can see the list
of unique dishes included.

Usage:

```bash
Welcome to the Restaurants Insights - 2019!
Write 'menu' at any moment to print the menu again.
---
1. List of restaurants included in the research filter by ['' | --type=string | --city=string]
2. List of unique dishes included in the research
3. Number and distribution (%) of users by [--group=[age | gender | occupation | nationality]]
4. Top 10 restaurants by the number of visitors.
5. Top 10 restaurants by the sum of sales.
6. Top 10 restaurants by the average expense of their users.
7. The average consumer expense group by [--group=[age | gender | occupation | nationality]]
8. The total sales of all the restaurants group by month [--order=asc]]
9. The list of dishes and the restaurant where you can find it at a lower price.
10. The favorite dish for [--age=number | --gender=string | --occupation=string | --nationality=string]
---
Pick a number from the list and an [option] if neccesary
>2
+-----------------------------+
|        List of dishes       |
+-----------------------------+
| name                        |
+-----------------------------+
| Arepas                      |
+-----------------------------+
| Pasta with Tomato and Basil |
+-----------------------------+
| Sushi                       |
+-----------------------------+
| ...                         |
+-----------------------------+
```

**Client can see the number and distribution of users**

As a client I want to use the option #3 of the menu so that I can see the list
the number and distribution of users group by age, gender, occupation or
nationality.

Usage:

```bash
Welcome to the Restaurants Insights - 2019!
Write 'menu' at any moment to print the menu again.
---
1. List of restaurants included in the research filter by ['' | --type=string | --city=string]
2. List of unique dishes included in the research
3. Number and distribution (%) of users by --group=[age | gender | occupation | nationality]
4. Top 10 restaurants by the number of visitors.
5. Top 10 restaurants by the sum of sales.
6. Top 10 restaurants by the average expense of their users.
7. The average consumer expense group by [--group=[age | gender | occupation | nationality]]
8. The total sales of all the restaurants group by month [--order=asc]]
9. The list of dishes and the restaurant where you can find it at a lower price.
10. The favorite dish for [--age=number | --gender=string | --occupation=string | --nationality=string]
---
Pick a number from the list and an [option] if neccesary
## group by age
>3 --group=age
+----------------------------------+
| Number and Distribution of Users |
+--------+----------+--------------+
| age    | count    | percentage   |
+--------+----------+--------------+
| 18     | 25       | 5%           |
+--------+----------+--------------+
| 19     | 50       | 10%          |
+--------+----------+--------------+
| 20     | 75       | 15%          |
+--------+----------+--------------+
| ...    | ...      | ...          |
+--------+----------+--------------+

## group by gender
>3 --group=gender
+----------------------------------+
| Number and Distribution of Users |
+----------+---------+-------------+
| gender   | count   | percentage  |
+----------+---------+-------------+
| Male     | 250     | 50%         |
+----------+---------+-------------+
| Female   | 250     | 50%         |
+----------+---------+-------------+

## group by occupation
>3 --group=occupation
+--------------------------------------+
|   Number and Distribution of Users   |
+-----------------+-------+------------+
| occupation      | count | percentage |
+-----------------+-------+------------+
| Artist          | 25    | 5%         |
+-----------------+-------+------------+
| Attorney at Law | 50    | 10%        |
+-----------------+-------+------------+
| Bartender       | 75    | 15%        |
+-----------------+-------+------------+
| ...             | ...   | ...        |
+-----------------+-------+------------+

## group by nationality
>3 --group=nationality
+-----------------------------------+
|  Number and Distribution of Users |
+--------------+-------+------------+
| nationality  | count | percentage |
+--------------+-------+------------+
| Albanians    | 25    | 5%         |
+--------------+-------+------------+
| Argentines   | 50    | 10%        |
+--------------+-------+------------+
| Bangladeshis | 75    | 15%        |
+--------------+-------+------------+
| ...          | ...   | ...        |
+--------------+-------+------------+
```

**Client can see the top 10 restaurants by the number of visitors**

As a client I want to use the option #4 of the menu so that I can see the top 10
restaurants by the number of visitors.

Usage:

```bash
Welcome to the Restaurants Insights - 2019!
Write 'menu' at any moment to print the menu again.
---
1. List of restaurants included in the research filter by ['' | --type=string | --city=string]
2. List of unique dishes included in the research
3. Number and distribution (%) of users by [--group=[age | gender | occupation | nationality]]
4. Top 10 restaurants by the number of visitors.
5. Top 10 restaurants by the sum of sales.
6. Top 10 restaurants by the average expense of their users.
7. The average consumer expense group by [--group=[age | gender | occupation | nationality]]
8. The total sales of all the restaurants group by month [--order=asc]]
9. The list of dishes and the restaurant where you can find it at a lower price.
10. The favorite dish for [--age=number | --gender=string | --occupation=string | --nationality=string]
---
Pick a number from the list and an [option] if neccesary
>4
+--------------------------------+
| Top 10 restaurants by visitors |
+--------------------+-----------+
| name               | visitors  |
+--------------------+-----------+
| Orange BBQ         | 350       |
+--------------------+-----------+
| Blue Plate Diner   | 300       |
+--------------------+-----------+
| Smokestack Dragon  | 250       |
+--------------------+-----------+
| ...                | ...       |
+--------------------+-----------+
```

**Client can see the top 10 restaurants by the sum of sales**

As a client I want to use the option #5 of the menu so that I can see the top 10
restaurants by the sum of sales.

Usage:

```bash
Welcome to the Restaurants Insights - 2019!
Write 'menu' at any moment to print the menu again.
---
1. List of restaurants included in the research filter by ['' | --type=string | --city=string]
2. List of unique dishes included in the research
3. Number and distribution (%) of users by [--group=[age | gender | occupation | nationality]]
4. Top 10 restaurants by the number of visitors.
5. Top 10 restaurants by the sum of sales.
6. Top 10 restaurants by the average expense of their users.
7. The average consumer expense group by [--group=[age | gender | occupation | nationality]]
8. The total sales of all the restaurants group by month [--order=asc]]
9. The list of dishes and the restaurant where you can find it at a lower price.
10. The favorite dish for [--age=number | --gender=string | --occupation=string | --nationality=string]
---
Pick a number from the list and an [option] if neccesary
>5
+-----------------------------+
| Top 10 restaurants by sales |
+--------------------+--------+
| name               | sales  |
+--------------------+--------+
| Smokestack Dragon  | 9500   |
+--------------------+--------+
| Blue Plate Diner   | 7500   |
+--------------------+--------+
| Fat Pizza          | 5500   |
+--------------------+--------+
| ...                | ...    |
+--------------------+--------+
```

**Client can see the top 10 restaurants by the average expense of their users**

As a client I want to use the option #6 of the menu so that I can see the top 10
restaurants by the average expense of their users.

Usage:

```bash
Welcome to the Restaurants Insights - 2019!
Write 'menu' at any moment to print the menu again.
---
1. List of restaurants included in the research filter by ['' | --type=string | --city=string]
2. List of unique dishes included in the research
3. Number and distribution (%) of users by [--group=[age | gender | occupation | nationality]]
4. Top 10 restaurants by the number of visitors.
5. Top 10 restaurants by the sum of sales.
6. Top 10 restaurants by the average expense of their users.
7. The average consumer expense group by [--group=[age | gender | occupation | nationality]]
8. The total sales of all the restaurants group by month [--order=asc]]
9. The list of dishes and the restaurant where you can find it at a lower price.
10. The favorite dish for [--age=number | --gender=string | --occupation=string | --nationality=string]
---
Pick a number from the list and an [option] if neccesary
>6
+------------------------------------------------+
| Top 10 restaurants by average expense per user |
+---------------------------+--------------------+
| name                      | avg expense        |
+---------------------------+--------------------+
| Big Eats                  | 50.5               |
+---------------------------+--------------------+
| Smokestack Dragon         | 48.3               |
+---------------------------+--------------------+
| Orange BBQ                | 45.6               |
+---------------------------+--------------------+
| ...                       | ...                |
+---------------------------+--------------------+
```

**Client can see the average consumer expense**

As a client I want to use the option #7 of the menu so that I can see the
average consumer expense group by age, gender, occupation or nationality

Usage:

```bash
Welcome to the Restaurants Insights - 2019!
Write 'menu' at any moment to print the menu again.
---
1. List of restaurants included in the research filter by ['' | --type=string | --city=string]
2. List of unique dishes included in the research
3. Number and distribution (%) of users by [--group=[age | gender | occupation | nationality]]
4. Top 10 restaurants by the number of visitors.
5. Top 10 restaurants by the sum of sales.
6. Top 10 restaurants by the average expense of their users.
7. The average consumer expense group by [--group=[age | gender | occupation | nationality]]
8. The total sales of all the restaurants group by month [--order=asc]]
9. The list of dishes and the restaurant where you can find it at a lower price.
10. The favorite dish for [--age=number | --gender=string | --occupation=string | --nationality=string]
---
Pick a number from the list and an [option] if neccesary
## group by age
>7 --group=age
+---------------------------+
| Average consumer expenses |
+---------+-----------------+
| age     | avg expense     |
+---------+-----------------+
| 30      | 50.5            |
+---------+-----------------+
| 25      | 48.3            |
+---------+-----------------+
| 45      | 45.6            |
+---------+-----------------+
| ...     | ...             |
+---------+-----------------+

## group by gender
>7 --group=gender
+---------------------------+
| Average consumer expenses |
+-----------+---------------+
| gender    | avg expense   |
+-----------+---------------+
| Male      | 50.5          |
+-----------+---------------+
| Female    | 48.3          |
+-----------+---------------+

## group by occupation
>7 --group=occupation
+---------------------------+
| Average consumer expenses |
+-------------+-------------+
| occupation  | avg expense |
+-------------+-------------+
| Electrician | 50.5        |
+-------------+-------------+
| Businessman | 48.3        |
+-------------+-------------+
| Hairdresser | 45.2        |
+-------------+-------------+
| ...         | ...         |
+-------------+-------------+

## group by nationality
>7 --group=nationality
+---------------------------+
| Average consumer expenses |
+-------------+-------------+
| nationality | avg expense |
+-------------+-------------+
| Colombians  | 50.5        |
+-------------+-------------+
| Quebecers   | 48.3        |
+-------------+-------------+
| Ukrainians  | 45.2        |
+-------------+-------------+
| ...         | ...         |
+-------------+-------------+
```

**Optional: Client can see the total sales of all the restaurants group by month**

As a client I want to use the option #8 of the menu so that I can see the total
sales of all the restaurants group by month.

Usage:

```bash
Welcome to the Restaurants Insights - 2019!
Write 'menu' at any moment to print the menu again.
---
1. List of restaurants included in the research filter by ['' | --type=string | --city=string]
2. List of unique dishes included in the research
3. Number and distribution (%) of users by [--group=[age | gender | occupation | nationality]]
4. Top 10 restaurants by the number of visitors.
5. Top 10 restaurants by the sum of sales.
6. Top 10 restaurants by the average expense of their users.
7. The average consumer expense group by [--group=[age | gender | occupation | nationality]]
8. The total sales of all the restaurants group by month [--order=asc]]
9. The list of dishes and the restaurant where you can find it at a lower price.
10. The favorite dish for [--age=number | --gender=string | --occupation=string | --nationality=string]
---
Pick a number from the list and an [option] if neccesary
## Without option the order is descendent by default
>8
+----------------------+
| Total sales by month |
+------------+---------+
| month      | sales   |
+------------+---------+
| June       | 100000  |
+------------+---------+
| April      | 85000   |
+------------+---------+
| December   | 70000   |
+------------+---------+
| ...        | ...     |
+------------+---------+

## ascendant order
>8 --order=asc
+----------------------+
| Total sales by month |
+-------------+--------+
| month       | sales  |
+-------------+--------+
| May         | 35000  |
+-------------+--------+
| October     | 38000  |
+-------------+--------+
| September   | 40000  |
+-------------+--------+
| ...         | ...    |
+-------------+--------+
```

**Optional: Client can see the best deal for a dish**

As a client I want to use the option #9 of the menu so that I can see the list
of dishes and the restaurant where you can find it at a lower price.

Usage:

```bash
Welcome to the Restaurants Insights - 2019!
Write 'menu' at any moment to print the menu again.
---
1. List of restaurants included in the research filter by ['' | --type=string | --city=string]
2. List of unique dishes included in the research
3. Number and distribution (%) of users by [--group=[age | gender | occupation | nationality]]
4. Top 10 restaurants by the number of visitors.
5. Top 10 restaurants by the sum of sales.
6. Top 10 restaurants by the average expense of their users.
7. The average consumer expense group by [--group=[age | gender | occupation | nationality]]
8. The total sales of all the restaurants group by month [--order=asc]]
9. The list of dishes and the restaurant where you can find it at a lower price.
10. The favorite dish for [--age=number | --gender=string | --occupation=string | --nationality=string]
---
Pick a number from the list and an [option] if neccesary
# The list is order by dish alphabetically
>9
+---------------------------------------------------+
|                Best price for dish                |
+------------------------+------------------+-------+
| dish                   | restaurant       | price |
+------------------------+------------------+-------+
| Arepas                 | Orange BBQ       | 18    |
+------------------------+------------------+-------+
| Barbecue Ribs          | Silver Brasserie | 20    |
+------------------------+------------------+-------+
| Bruschette with Tomato | Golden Curry     | 19    |
+------------------------+------------------+-------+
| ...                    | ...              | ...   |
+------------------------+------------------+-------+
```

**Optional: Client can see favorite dish for a group of users**

As a client I want to use the option #10 of the menu so that I can see the
favorite dish filter by and specific age, gender, occupation or nationality

Usage:

```bash
Welcome to the Restaurants Insights - 2019!
Write 'menu' at any moment to print the menu again.
---
1. List of restaurants included in the research filter by ['' | --type=string | --city=string]
2. List of unique dishes included in the research
3. Number and distribution (%) of users by [--group=[age | gender | occupation | nationality]]
4. Top 10 restaurants by the number of visitors.
5. Top 10 restaurants by the sum of sales.
6. Top 10 restaurants by the average expense of their users.
7. The average consumer expense group by [--group=[age | gender | occupation | nationality]]
8. The total sales of all the restaurants group by month [--order=asc]]
9. The list of dishes and the restaurant where you can find it at a lower price.
10. The favorite dish for [--age=number | --gender=string | --occupation=string | --nationality=string]
---
Pick a number from the list and an [option] if neccesary
## filter by age
>10 --age=35
+----------------------+
|     Favorite dish    |
+-----+--------+-------+
| age | dish   | count |
+-----+--------+-------+
| 35  | Arepas | 18    |
+-----+--------+-------+

## filter by gender
>10 --gender=Female
+-------------------------+
|      Favorite dish      |
+--------+--------+-------+
| gender | dish   | count |
+--------+--------+-------+
| Female | Arepas | 18    |
+--------+--------+-------+

## filter by occupation
>10 --occupation=Electrician
+------------------------------+
|         Favorite dish        |
+-------------+--------+-------+
| occupation  | dish   | count |
+-------------+--------+-------+
| Electrician | Arepas | 18    |
+-------------+--------+-------+

## filter by nationality
>10 --nationality=Colombians
+------------------------------+
|         Favorite dish        |
+-------------+--------+-------+
| nationality | dish   | count |
+-------------+--------+-------+
| Colombians  | Arepas | 18    |
+-------------+--------+-------+
```

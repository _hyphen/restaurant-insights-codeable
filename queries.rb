# frozen_string_literal: true

require 'pg'

class RestaurantQueries
  attr_reader :conn

  def initialize(db_name)
    @conn = PG.connect(dbname: db_name)
    puts "Database #{@conn.db} connected on port #{@conn.port}"
  rescue PG::ConnectionBad
		puts "ERROR	: database \"#{db_name}\" unfound"
		File.delete('db_name.sql')
    exit(1)
  end
  # Thank you, Victor!
	

	ITERATOR = lambda do |title|
		lambda do |result| 
			rows = result.map { |row| row.values } 		 
			return if result.ntuples == 0 
			puts (Terminal::Table.new :title => "#{title}", :headings => result[0].keys, :rows => rows) 
		end 	
	end
	
	
	def list_unq_restaurant(option:, value:)
    condition = option.nil? ? '' : " WHERE #{option} = '#{value}'"
    conn.exec(%(SELECT DISTINCT name, type, city FROM "Restaurant"#{condition}), &ITERATOR.('List of Restaurants'))
  end

  def list_unq_dish
    conn.exec(%(SELECT DISTINCT name FROM "Dish"), &ITERATOR.('List of Dishes'))
  end

  def list_client_group_by(value:)
    conn.exec(%(SELECT #{value}, COUNT(*) FROM "Client"
		GROUP BY #{value}), &ITERATOR.('Number and Distribution of Users')) # Agregar el porcentaje con ruby
  end

  def top10_by_visits
    conn.exec(%(SELECT r.name, COUNT(*) AS visitors
		FROM "Client" AS c
		JOIN "Visit" AS v ON v.client_id = c.id
		JOIN "Restaurant" AS r ON r.id  = v.rest_id
		GROUP BY r.name
		ORDER BY visitors DESC
		LIMIT 10), &ITERATOR.('Top 10 restaurants by visitors'))
  end

  def top10_by_total_sales
    conn.exec(%(SELECT r.name, SUM(d.price) AS sales
		FROM "Client" AS c
		JOIN "Visit" AS v ON v.client_id = c.id
		JOIN "Restaurant" AS r ON r.id  = v.rest_id
		JOIN "Dish" AS d ON d.id = v.dish_id
		GROUP BY r.name
		ORDER BY sales DESC
		LIMIT 10), &ITERATOR.('Top 10 restaurants by sales'))
  end

  def top10_by_avg_expenses
    conn.exec(%(SELECT r.name, ROUND(AVG(d.price), 1) AS "avg expense"
		FROM "Client" AS c
		JOIN "Visit" AS v ON v.client_id = c.id
		JOIN "Restaurant" AS r ON r.id  = v.rest_id
		JOIN "Dish" AS d ON d.id = v.dish_id
		GROUP BY r.name
		ORDER BY "avg expense" DESC
		LIMIT 10), &ITERATOR.('Top 10 restaurants by average expense per user'))
  end

  def avg_expense_group_by(value:)
    conn.exec(%(SELECT c.#{value}, ROUND(AVG(d.price), 1) as "avg expense"
		FROM "Client" as c
		JOIN "Visit" as v ON c.id = v.client_id
		JOIN "Dish" as d ON d.id =  v.dish_id
		GROUP BY c.#{value};), &ITERATOR.('Average consumer expenses'))
	end
	
	def total_sales_by_month(value:)
		conn.exec(%(SELECT extract(month from v.date) AS "month", SUM(d.price) AS sale FROM "Restaurant" AS r
		JOIN "Visit" AS v 
		ON r.id = v.rest_id
				JOIN "Dish" AS d 
					ON v.dish_id = d.id
		GROUP BY "month"
		ORDER BY sale #{value}), &ITERATOR.('Total sales by month'))
	end

	def list_cheapest_dishes
		conn.exec(%|SELECT  cheapest_dishes.name AS dish, "Restaurant".name AS restaurant, cheapest_dishes.price 
		FROM "Restaurant" 
		JOIN 
		(SELECT  max(rest_id) AS rest_id, name, min(price) AS price 
		FROM "Dish" 
		GROUP BY "name") cheapest_dishes 
		ON "Restaurant".id = cheapest_dishes.rest_id 
		ORDER BY price|, &ITERATOR.('Best price for dish'))
	end

	def favorite_dish(option:, value:)
    value = value.upcase[0] if option == 'gender'
    value = "'#{value}'" unless option == 'age'
    conn.exec(%(SELECT #{option},"Dish".name, count(*) 
		FROM "Client" 
		JOIN "Visit" 
		ON "Visit".client_id = "Client".id 
		JOIN "Dish" 
		ON "Dish".id = "Visit".dish_id 
		WHERE #{option}=#{value} 
		GROUP BY #{option}, "Dish".name 
		ORDER BY count DESC LIMIT 1), &ITERATOR.('Favorite dish')) 
	end
end
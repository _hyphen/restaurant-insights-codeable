require 'pg'
require 'csv'

class Insert
  
  def initialize
    input
    connect_to_db
  end

  def input 
    @database_name = ARGV[0]
    @csv_file_path = ARGV[1]
  end

  def connect_to_db 
    begin
    @comn = PG.connect(dbname: @database_name)
    puts "You are connected to #{@database_name} on port #{@comn.port}" if @comn
  rescue
    puts "Database #{@database_name} does not exist"
    
    end
  end

  def insert_data
    file = CSV.read(@csv_file_path, headers: true, encoding: "UTF-8")
    file.each do |row| 
      insert_client(row['user_name'], row['age'], row['gender'], row['nationality'], row['occupation']) 
      insert_restaurant(row['restaurant_name'], row['city'], row['address'], row['type']) 
      rest_id = get_rest_id(row['address'])
      insert_dish(rest_id, row['dish'], row['price']) 
      insert_visit(get_client_id(row['user_name']), rest_id, get_dish_id(rest_id, row['dish']), row['visit_date']) 
    end
    File.open('db_name.sql', 'w') { |f| f.write(@database_name) }
  end   
    
  def insert_client(name, age, gender, nationality, occupation)
    wrap = lambda { |nop| "E'#{nop.gsub("'", "\\\\'")}'"}
    begin
      @comn.exec (%| SELECT * FROM "Client" 
        WHERE name = #{wrap.(name)} 
     |) do |x| return unless x.ntuples == 0 end;
    rescue => e
    end

    begin
      @comn.exec (%| 
        INSERT INTO "Client"
        (name, age, gender, nationality, occupation)
        VALUES
        (#{[wrap.(name), age, wrap.(gender[0]), wrap.(nationality), wrap.(occupation)].join(", ")})
      |);
    rescue => e  
      p e      
    end
  end  

  def insert_restaurant(name, city, address, type)
    wrap = lambda { |nop| "E'#{nop.gsub("'", "\\\\'")}'"}
    begin
      @comn.exec (%| SELECT * FROM "Restaurant" 
        WHERE address = #{wrap.(address)} 
     |) do |x| return unless x.ntuples == 0 end;
    rescue => e
    end

    begin
      @comn.exec (%| 
        INSERT INTO "Restaurant"
        (city, name, address, type)
        VALUES
        (#{[wrap.(city), wrap.(name), wrap.(address), wrap.(type)].join(", ")})
      |);
    rescue => e  
    end
  end

  def get_rest_id(address)
    @comn.exec (%| SELECT id FROM "Restaurant"
      WHERE address = E'#{address.gsub("'", "\\\\'")}'
      |) do |x| x[0]['id'].to_i end;
  end

  def insert_dish(rest_id, name, price)
    wrap = lambda { |nop| "E'#{nop.gsub("'", "\\\\'")}'"}
    
    begin
      @comn.exec (%| 
        INSERT INTO "Dish"
        (rest_id, name, price)
        VALUES
        (#{[rest_id, wrap.(name), price].join(", ")})
      |);
    rescue => e  
    end
  end

  def get_client_id(name)
    @comn.exec (%| SELECT id FROM "Client"
      WHERE name = E'#{name.gsub("'", "\\\\'")}'
      |) do |x|  x[0]['id'].to_i end;
  end

  def get_dish_id(rest_id, name)
    @comn.exec (%| SELECT id FROM "Dish"
      WHERE rest_id = #{rest_id} AND name = E'#{name.gsub("'", "\\\\'")}'
      |) do |x| x[0]['id'].to_i end;
  end

  def insert_visit(client_id, rest_id, dish_id, date)
    wrap = lambda { |nop| "E'#{nop.gsub("'", "\\\\'")}'"}
    
    begin
      @comn.exec (%| 
        INSERT INTO "Visit"
        (client_id, rest_id, dish_id, date)
        VALUES
        (#{[client_id, rest_id, dish_id, wrap.(date)].join(", ")})
      |);
    rescue => e 
    end
  end
  
end

test = Insert.new
test.insert_data



